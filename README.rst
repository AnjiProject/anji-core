============================
RethinkDB Storage for ErrBot
============================


.. image:: https://img.shields.io/pypi/v/anji-core.svg
        :target: https://pypi.python.org/pypi/anji-core

Core for AnjiProject, framework that build on top of ErrBot and provide simple way to control servers use ChatOps on many servers in time


* Free software: MIT license


Features
--------

* Use RethinkDB as data store
* Executing tasks on workers
* Cron tasks
* Event collector
* Much more ...

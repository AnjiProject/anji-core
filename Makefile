cron:
	ANJI_BOT_TYPE=cron_worker ANJI_BOT_NAME=local-cron errbot
worker:
	ANJI_BOT_NAME=local-worker errbot
master:
	ANJI_BOT_TYPE=master ANJI_BOT_NAME=local-master errbot
check:
	pylint anji_core
	pycodestyle anji_core
	restructuredtext-lint README.rst
	python setup.py checkdocs
test:
	pytest tests
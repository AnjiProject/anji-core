#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

setup(
    name='anji-core',
    version='0.8.11',
    description="Core of AnjiProject, desctibuted ChatOps system",
    long_description=readme,
    author="Bogdan Gladyshev",
    author_email='siredvin.dark@gmail.com',
    url='https://gitlab.com/AnjiProject/anji-core',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        "websocket-client==0.40.0",
        "errbot-rethinkdb-storage>=1.1.1",
        "trafaret-config>=1.0.0",
        "sarge>=0.1.4",
        "croniter>=0.3.17",
        "humanize>=0.5.1",
        "raven>=6.0.0",
        "requests>=2.18.0",
        "click>=6.7",
        "click-log>=0.2.1",
        "coloredlogs>=7.3.1",
        "python-consul>=0.7.2",
        'anji-orm>=0.4.4',
        "docker>=2.4.2",
        "docker_compose>=1.18.0"
    ],
    license="MIT license",
    zip_safe=False,
    keywords='chatops errbot slack telegram',
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],
    entry_points={
        'console_scripts': [
            'anji=anji_core.cli:cli',
        ],
    },
    tests_require=[],
    setup_requires=[],
)

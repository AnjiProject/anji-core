.. Anji-tan documentation master file, created by
   sphinx-quickstart on Tue Aug  8 07:33:09 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Anji-tan's documentation!
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. automodule:: anjilib.core.cartridges
   :members:

.. automodule:: anjilib.core.model
   :members:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

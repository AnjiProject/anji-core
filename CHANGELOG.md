# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.8.11] - 2018-02-01

### Fixed

- Describe command for flow

## [0.8.10] - 2018-02-01

### Fixed

- Now worker will always fetch service list

## [0.8.9] - 2018-02-01

### Added

- Healthy check report thread

### Fixed

- Delayed task processing (list and remove logic)

## [0.8.8] - 2018-01-26

### Added

- New signals `update` and `destroy`

### Changed

- Signal message and processing

## [0.8.7] - 2018-01-26

### Added

- Ability to change default `BOT_ALT_PREFIXES` via env variable

## [0.8.6] - 2018-01-25

### Fixed

- Task list problem

## [0.8.5] - 2018-01-25

### Fixed

- Fix python package build

## [0.8.4] - 2018-01-25 [YANKED]

### Added

- Ability to fetch process pids from service
- Migration for services
- New service type: `compose` for docker-compose project as service

### Changed

- Rename docker and systemd service modules
- Change signals policy: instead of status use is\_exists and is\_running functions
- Move to requirements-dev.txt again
- Unknow status now return info message type for services

### Fixed

- Cartridge init
- Field view and service creation

## [0.8.2] - 2017-12-20

### Changed

- Use Pipfile instead of requirements.txt

### Fixed

- Package build

## [0.8.1] - 2017-12-18 [YANKED]

### Added

- Advanced logic to delay regular task (#115)
- Ability to use pre and post hook after manager modification (#116)
- Message about cron list refresh to cron workers (#116)
- Support of external message proessing (#118)

### Changed

- New logic to work with regular tasks (#115)
- Cron task now use internal timer intead of database pulling (#116)
- Cron node can be refreshed via internal message (#116)
- Index creation on remove policy with additonal refactor
- AnjiORM was moved to external package
- Migrate to `repool-forked` instead of `repool` to use pypi repository

### Fixed

- Use magic number to store default thread sleep
- Query building with sort by field name
- Cron reset
- Regresion with `self.bot` usage
- Node registration (#117)
- Compitability with anjiorm

## [0.8.0] - 2017-12-04

### Added

- New help command by cartridge (#113)
- Manager env and automatic manager load (#102)
- Entrypoint for cli command in package
- Support of external library usage

### Changed

- Now slack use markdown rendering
- Now field list use ast instead of underline
- Now managers cannot be accessed directly from bot object, please, use manager_env property
- Migrated from plugin usage to bot modification
- Left only core code in anjilib
- Rename anjilib to anji-core
- Transform respotitory to real python package
- Use internal configuration
- **Breaking changes**: orm model not use bot currently (#114)

## [0.7.7] - 2017-10-30

### Fixed

- Bash task processing

## [0.7.6] - 2017-10-30

### Fixed

- Signals argument processing

## [0.7.5] - 2017-10-30

### Fixed

- Internal message processing

## [0.7.4] - 2017-10-30

### Fixed

- Node name usage

## [0.7.3] - 2017-10-30

### Fixed

- Cron migrate fix again!

## [0.7.2] - 2017-10-30

### Changed

- Now cron migrate task use limit

## [0.7.1] - 2017-10-30

### Changed

- Remove not ready event collector and consumer from config.py
- Only master node can be used for database migration (#112)

## [0.7.0] - 2017-10-30

### Added

- Ability to approve user and register in system
- Support for security usage in flow (#108)
- New event subsystem

### Changed

- **Breaking changes**: Total rework of manager system, now manager generate commands and control logic
- **Breaking changes**: Total rework of tasks, now tasks store args like fields
- More and more stuff moved to build_query function, orm usage improved
- **Breaking changes**: now every type is enum
- **Breaking changes**: User model changes, now support name and technical_name for this model
- Improve build query and orm to use combine secondary indexes
- Changelog accroding to [Keep a Changelog](http://keepachangelog.com/en/1.0.0/) (#110)

## [0.6.14] - 2017-10-21

### Added

- Ability to set docker restart and stop timeout
- Ability to use selection on flow

## [0.6.13] - 2017-10-17

### Fixed

- `odoo update modules` command to not split modules again!

## [0.6.12] - 2017-10-17

### Fixed

- `odoo update modules` command to not split modules

## [0.6.11] - 2017-10-17

### Added

- Reactions for detailed and last-result report
- Tests (#50)

### Changed

- Task result will print before next command processing
- Prettify `!node list` output
- Prettify `!cron list` output
- `!cron list` request to use index

### Fixed

- Title in task detailed report

## [0.6.10] - 2017-10-13

### Added

- Fetch service list before task execution (#98)
- Alternative text option for message reaction signal
- Allow branch managment from bot (#43)
- Security for services usage (#70)
- FAQ for reactions (#100)

### Changed

- Cartridge configuration commands now use reactions
- Fields for ORM models become descriptors (#86)
- Internal message now use index by status (#101)
- Backend now use python MRO (#99)
- Separate service list to message per service

### Fixed

- Command syntax for odoo install and update modules (#92)
- `!cron list` command
- Logic on container restart
- Cron task stuck calculation

## [0.6.9] - 2017-10-03

### Changed

- Node registraction policy (#91)

### Fixed

- Problem with simple argument in decorators

## [0.6.8] - 2017-09-28

### Added

- Offset to compress functions (#95)
- Automatic secondary index and fix searches by this indexes (#96)
- More complex additional statistic filter for task to solve index usage problem

### Fixed

- Cron task fail processing (#93)
- Cron reset filter

## [0.6.7] - 2017-09-26

### Added

- Prediction of execution time (#53)
- Ability to compress timeseries modelss (#89)

### Removed

- Remove useless log tables (#88)

### Fixed

- Critical bag with default values for fields
- Task datetime execution metrics

## [0.6.6] - 2017-09-20

### Added

- Internal comunication between nodes and change flow mode to this communication (#82)

### Fixed

- Telegram backend, it's now ugly, but work (#80)
- worker_hostname paramater processing

## [0.6.5] - 2017-09-18

### Added

- Make signal for task failed
- Add new signal to odoo install and update modules commands

### Changed

- Rework notify system to signals, add global user table (#85)
- Migrate to new decorator for tasks
- Rework some commands to work with reply

### Fixed

- Signals for flow
- Cron task repotring, again!

## [0.6.4] - 2017-09-13

### Added

- Event notifier (#84)

### Fixed

- Filter for canceling task, !task cancel number now cancel only selected task

## [0.6.3] - 2017-09-11

### Changed

- Base docker image

### Fixed

- New flow processing in chat rooms (#83)

## [0.6.2] - 2017-09-10

### Added

- Ability to remove service and add new cartridge confirmation feature (#72)
- Command to check cartridge settings (#77)
- Ability to get last task result again (#32)
- Automatic information to services (#73)
- Event manager and reactions support (#79, #55)

### Changed

- Simplify cartridges creation with help of decorators and add ability to remove cron task (#76)

### Fixed

- Last executime time in cron task (#75)

## [0.6.1] - 2017-09-10

### Changed

- Disable sending before service list loading

### Fixed

- Hot fix for cron task creation
- Argument order in generated commands
- Condition to create command for cartridge configuration
- Cartridge settings update

## [0.6.0] - 2017-09-10

### Added

- Some command to cron task manipulating (#61)
- `node list` and `node cleanup` commands (#62)
- Commands for cartridge initialization (#69)
- Basic readme (#68)

### Changed

- Move to rethinkdb pool usage (#49)
- Migrate most of configuration to RethinkDB (#63)
- Update odoo tasks to install and remove not all modules (#13)
- Create metaclass for cartridges (#65)

### Fixed

- Title rendering (#59)
- Git update command with pipe `fetch->reset->pull` (#52)


[Unreleased]: https://gitlab.com/AnjiProject/anji-core/compare/v0.8.10...HEAD
[0.8.10]: https://gitlab.com/AnjiProject/anji-core/compare/v0.8.9...v0.8.10
[0.8.9]: https://gitlab.com/AnjiProject/anji-core/compare/v0.8.8...v0.8.9
[0.8.8]: https://gitlab.com/AnjiProject/anji-core/compare/v0.8.7...v0.8.8
[0.8.7]: https://gitlab.com/AnjiProject/anji-core/compare/v0.8.6...v0.8.7
[0.8.6]: https://gitlab.com/AnjiProject/anji-core/compare/v0.8.5...v0.8.6
[0.8.5]: https://gitlab.com/AnjiProject/anji-core/compare/v0.8.4...v0.8.5
[0.8.4]: https://gitlab.com/AnjiProject/anji-core/compare/v0.8.3...v0.8.4
[0.8.3]: https://gitlab.com/AnjiProject/anji-core/compare/v0.8.2...v0.8.3
[0.8.2]: https://gitlab.com/AnjiProject/anji-core/compare/v0.8.1...v0.8.2
[0.8.1]: https://gitlab.com/AnjiProject/anji-core/compare/v0.8.0...v0.8.1
[0.8.0]: https://gitlab.com/AnjiProject/anji-core/compare/v0.7.7...v0.8.0
[0.7.7]: https://gitlab.com/AnjiProject/anji-core/compare/v0.7.6...v0.7.7
[0.7.6]: https://gitlab.com/AnjiProject/anji-core/compare/v0.7.5...v0.7.6
[0.7.5]: https://gitlab.com/AnjiProject/anji-core/compare/v0.7.4...v0.7.5
[0.7.4]: https://gitlab.com/AnjiProject/anji-core/compare/v0.7.3...v0.7.4
[0.7.3]: https://gitlab.com/AnjiProject/anji-core/compare/v0.7.2...v0.7.3
[0.7.2]: https://gitlab.com/AnjiProject/anji-core/compare/v0.7.1...v0.7.2
[0.7.1]: https://gitlab.com/AnjiProject/anji-core/compare/v0.7.0...v0.7.1
[0.7.0]: https://gitlab.com/AnjiProject/anji-core/compare/v0.6.14...v0.7.0
[0.6.14]: https://gitlab.com/AnjiProject/anji-core/compare/v0.6.13...v0.6.14
[0.6.13]: https://gitlab.com/AnjiProject/anji-core/compare/v0.6.12...v0.6.13
[0.6.12]: https://gitlab.com/AnjiProject/anji-core/compare/v0.6.11...v0.6.12
[0.6.11]: https://gitlab.com/AnjiProject/anji-core/compare/v0.6.10...v0.6.11
[0.6.10]: https://gitlab.com/AnjiProject/anji-core/compare/v0.6.9...v0.6.10
[0.6.9]: https://gitlab.com/AnjiProject/anji-core/compare/v0.6.8...v0.6.9
[0.6.8]: https://gitlab.com/AnjiProject/anji-core/compare/v0.6.7...v0.6.8
[0.6.7]: https://gitlab.com/AnjiProject/anji-core/compare/v0.6.6...v0.6.7
[0.6.6]: https://gitlab.com/AnjiProject/anji-core/compare/v0.6.5...v0.6.6
[0.6.5]: https://gitlab.com/AnjiProject/anji-core/compare/v0.6.4...v0.6.5
[0.6.4]: https://gitlab.com/AnjiProject/anji-core/compare/v0.6.3...v0.6.4
[0.6.3]: https://gitlab.com/AnjiProject/anji-core/compare/v0.6.2...v0.6.3
[0.6.2]: https://gitlab.com/AnjiProject/anji-core/compare/v0.6.1...v0.6.2
[0.6.1]: https://gitlab.com/AnjiProject/anji-core/compare/v0.6.0...v0.6.1
[0.6.0]: https://gitlab.com/AnjiProject/anji-core/compare/0.5.1...v0.6.0